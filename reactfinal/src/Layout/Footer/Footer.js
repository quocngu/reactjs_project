import React, { Component } from 'react'

export default class Footer extends Component {
    render() {
        return (
            <>
                <footer className="footer">
                    <div className="container content">
                        <div className="row">
                            <div className="col-3 address">
                                <h3>COURSECTOR ACADEMY</h3>
                                <h5>Hệ thống đạo tạo lập trình chuyên nghiệp</h5>
                                <p>-82 Ung Văn Khiêm - Bình Thạch</p>
                                <p>Phone: 070-376-4616</p>
                                <p>Fax: 09384-1374748</p>
                                <p>Email:quocngu4616@gmail.com</p>
                            </div>
                            <div className="col-3 bonus">
                                <h3>ƯU ĐÃI THÁNG 12</h3>
                                <p>2 suất MIỄN PHÍ 100% Khóa học front-end và nhiều ưu đãi giảm giá khi đăng kí tại chi nhắn 82- UNG
            VĂN KHIÊM và 459- SƯ VẠN HẠNH </p>
                            </div>
                            <div className="col-6 sub">
                                <h3>ĐĂNG KÍ ĐỂ ĐƯƠC TƯ VẤN MIỄN PHÍ</h3>
                                <div className="form-group ">
                                    <input type="text" className="form-control" name placeholder="Email liên hệ" />
                                    <button className="btn btn--white text-center">SUBMIT</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
            </>
        )
    }
}
