import React, { Component } from 'react'
import SearchCouse from '../../Containers/SearchCouse'
import { Link, NavLink } from 'react-router-dom'
import { connect } from 'react-redux'
import { notify } from '../../Services/notify/Notify'
import _ from 'lodash'
import { createAction } from '../../Redux/Action'
import { SET_CREDENTIALS_EMPTY } from '../../Redux/Action/type'
import { fetchUsers } from '../../Redux/Action/user'

class Header extends Component {
    componentDidMount() {
        this.props.dispatch(fetchUsers())
    }
    checkShowAccount = (userList) => {
        let credentials = JSON.parse(localStorage.getItem('credentials'));
        console.log("header", credentials);
        // 
        if (_.isEmpty(userList) && !credentials) {
            return (
                <div className="button d-flex">
                    <Link to="/login"> <button className="btn btn--white mr-3">Đăng Nhập</button></Link>
                    <Link to="/signin"><button className="btn btn--red">Đăng Kí</button></Link>
                </div>
            )
        }
        else {
            return (
                <div className="user">
                    <Link to="./info">
                        <div className="user__avatar"><img src="/img/avatar.png" alt /></div>
                    </Link>
                    <div className="logout__btn"><span className="lnr lnr-power-switch" onClick={() => this.handleLogout()} /></div>
                </div>
            )
        }
    }
    showSetting = (userList) => {
        // let credentials=JSON.parse(localStorage.getItem('credentials'));
        if (userList.maLoaiNguoiDung === "HV") {

        }
        else if (userList.maLoaiNguoiDung === "GV") {
            return (
                <Link to="/admin">
                    <div className="admin__panel"><span className="lnr lnr-cog" /></div>
                </Link>
            )
        }

    }
    handleLogout = () => {
        this.props.dispatch(createAction(SET_CREDENTIALS_EMPTY, {})) //set lại credential của user trống
        localStorage.removeItem('credentials');
        notify('success', 'ĐĂNG XUẤT THÀNH CÔNG');
        this.props.history.push('./')
    }
    render() {
        let { userList,history } = this.props
        console.log(userList);

        return (

            <header className="container-fluid myNavBar bg-light">
                <nav className="navbar navbar-expand-sm navbar-light px-2 py-1">
                    <div className="col-xl-6 col-lg-9 col-md-8">
                        <div className="row">
                            <Link to="/" className="navbar-brand" href="#">
                                <img src="./img/logo2.png" alt="không hiển thi ảnh" />
                            </Link>
                            <div className="input_search">
                                {/* sử dụng input group */}
                                <SearchCouse history={history} />
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-6 col-md-6 col-lg-6 col-xl-6 ">
                        <div className="menu__right">
                            <div className="cart"><Link to="/detailcart"><i className="lnr lnr-cart" /><span className="cart__quantity">{this.props.cartList.length}</span></Link></div>
                            <div className="noti"><span className="lnr lnr-alarm" /><span className="noti__quantity">5</span></div>
                            {/* admin */}
                            {this.showSetting(this.props.userList)}

                            {this.checkShowAccount(this.props.userList)}



                        </div>
                    </div>

                </nav>
            </header>

        )
    }
}
const mapStateToProps = state => ({
    cartList: state.cart,
    userList: state.user.credentials
})
export default connect(mapStateToProps, null)(Header)