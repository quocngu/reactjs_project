import React, { Component } from 'react'
import Header from '../Layout/Header/Header'
import DetailContent from '../Layout/DetailContent/DetailContent'
import Footer from '../Layout/Footer/Footer'
import { fetchCourseDetail } from '../Redux/Action/course'
// import { fetchCourseDetail } from '../Redux/Action/course'
import {connect} from 'react-redux'
class DetaiItem extends Component {
    componentDidMount() {
        this.props.dispatch(fetchCourseDetail(this.props.match.params.maKhoaHoc))
    }
    render() {
        return (
            <>
                {/* header */}
                <Header history={this.props.history}/>
                {/* contentDetail */}
                <DetailContent />
                {/* footer */}
                <Footer />


            </>
        )
    }
}
export default connect()(DetaiItem)