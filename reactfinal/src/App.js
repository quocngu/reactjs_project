import React from 'react';
import './Styles/main.scss'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import Home from './Screens/Home';
import Detail from './Screens/Detail';
import DetailItem from './Screens/DetaiItem';
import Signin from './Screens/Signin';
import Login from './Screens/Login';
import DetailContent from './Layout/DetailContent/DetailContent';
import DetailCart from './Screens/DetailCart';
import Info from './Screens/Info';
import Admin from './Screens/Admin';



function App() {
  return (

    // <Admin/>
    // <AdminUser/>

    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/info" exact component={Info} />
        <Route path="/detail" exact component={Detail} />
        <Route path="/detailcontent/:maKhoaHoc" exact component={DetailItem} />
        <Route path="/signin" exact component={Signin} />
        <Route path="/login" exact component={Login} />
        <Route path="/detailcart" exact component={DetailCart} />
        <Route path="/admin" exact component={Admin} />
      </Switch>



    </BrowserRouter>





  );
}

export default App;
